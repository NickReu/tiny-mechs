module MyCss exposing (..)

import Css exposing (..)
import Css.Colors exposing (green)
import Html.Styled exposing (..)
import Html.Styled.Attributes exposing (css, href, src)


columns : List (Html msg) -> Html msg
columns cs =
    div [ css [ displayFlex, flexDirection row, width (pct 100) ] ] cs


column : Html msg -> Float -> Html msg
column content weight =
    div [ css [ flexGrow (num weight) ] ] [ content ]


crispImage : String -> Float -> Float -> Html msg
crispImage url h w =
    img [ src url, css [ property "image-rendering" "crisp-edges", property "image-rendering" "pixelated", height (px h), width (px w) ] ] []


heightCentered : Html msg -> Float -> Html msg
heightCentered content h =
    div [ css [ height (px h) ] ] [ centeredFlex content ]


centeredFlex : Html msg -> Html msg
centeredFlex content =
    centeredCenter
        (div
            [ css
                [ displayFlex
                , justifyContent center
                , alignItems center
                , padding (px 10)
                ]
            ]
            [ content ]
        )


centeredCenter : Html msg -> Html msg
centeredCenter content =
    div
        [ css
            [ displayFlex
            , justifyContent center
            , alignItems center
            ]
        ]
        [ content ]
