module Main exposing (Model(..), Msg(..), init, main, subscriptions, update, view)

import Browser
import Character exposing (..)
import Css
import Html
import Html.Styled exposing (Html, button, div, form, img, input, label, text)
import Html.Styled.Attributes exposing (css, height, name, src, type_, width)
import Html.Styled.Events exposing (onClick, onInput, onSubmit)
import Http
import Json.Encode as Encode
import MyCss exposing (..)



-- MAIN


main =
    Browser.document
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }



-- MODEL


type Model
    = Failure String
    | Loading
    | Genned Character String
    | Show Character


init : () -> ( Model, Cmd Msg )
init _ =
    ( Loading
    , Http.request
        { url = "@@@@@/gen/char"
        , method = "GET"
        , expect = Http.expectJson GennedChar charDecoder
        , headers = []
        , body = Http.emptyBody
        , timeout = Nothing
        , tracker = Nothing
        }
    )



-- UPDATE


type Msg
    = GenChar
    | GennedChar (Result Http.Error Character)
    | ChangePlayer String
    | SaveChar String
    | SavedChar (Result Http.Error Character)
    | FetchChar String
    | FetchedChar (Result Http.Error Character)


handleError : Http.Error -> ( Model, Cmd Msg )
handleError err =
    case err of
        Http.BadUrl url ->
            ( Failure ("unable to reach url " ++ url), Cmd.none )

        Http.Timeout ->
            ( Failure "network timeout", Cmd.none )

        Http.NetworkError ->
            ( Failure "network error", Cmd.none )

        Http.BadStatus status ->
            ( Failure ("error " ++ String.fromInt status), Cmd.none )

        Http.BadBody body ->
            ( Failure ("error: " ++ body), Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GennedChar result ->
            case result of
                Ok char ->
                    ( Genned char "", Cmd.none )

                Err err ->
                    handleError err

        SavedChar result ->
            case result of
                Ok char ->
                    ( Show char, Cmd.none )

                Err err ->
                    handleError err

        FetchedChar result ->
            case result of
                Ok char ->
                    ( Show char, Cmd.none )

                Err err ->
                    handleError err

        GenChar ->
            ( model
            , Http.request
                { url = "@@@@@/gen/char"
                , method = "GET"
                , expect = Http.expectJson GennedChar charDecoder
                , headers = []
                , body = Http.emptyBody
                , timeout = Nothing
                , tracker = Nothing
                }
            )

        SaveChar player ->
            case model of
                Genned c p ->
                    let
                        body : Encode.Value
                        body =
                            Encode.object
                                [ ( "player", Encode.string player ) ]
                    in
                    ( model
                    , Http.request
                        { url = "@@@@@/save/char/" ++ c.charId
                        , method = "POST"
                        , expect = Http.expectJson SavedChar charDecoder
                        , headers = []
                        , body = Http.jsonBody body
                        , timeout = Nothing
                        , tracker = Nothing
                        }
                    )

                _ ->
                    ( model, Cmd.none )

        ChangePlayer player ->
            case model of
                Genned character _ ->
                    ( Genned character player
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        FetchChar id ->
            ( model
            , Http.request
                { url = "@@@@@/char/" ++ id
                , method = "GET"
                , expect = Http.expectJson FetchedChar charDecoder
                , headers = []
                , body = Http.emptyBody
                , timeout = Nothing
                , tracker = Nothing
                }
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


summary : Character -> Float -> Html Msg
summary char imgSize =
    div []
        [ portrait char imgSize
        , centeredFlex
            (div
                []
                [ text "Stats"
                , Html.Styled.ul []
                    [ Html.Styled.li [] [ text ("Spark: " ++ String.fromInt char.stats.spark) ]
                    , Html.Styled.li [] [ text ("Steel: " ++ String.fromInt char.stats.steel) ]
                    ]
                ]
            )
        ]


portrait : Character -> Float -> Html Msg
portrait char imgSize =
    div []
        [ centeredFlex (crispImage char.imgUrl imgSize imgSize)
        , centeredFlex
            (text ("The " ++ char.charName))
        ]


view : Model -> Browser.Document Msg
view model =
    { title = "Tiny Mechs"
    , body =
        List.map Html.Styled.toUnstyled
            [ case model of
                Failure message ->
                    centeredFlex (text ("Failed to load: " ++ message))

                Loading ->
                    centeredFlex (text "Loading...")

                Genned char player ->
                    div []
                        [ portrait char 200
                        , centeredFlex
                            (button [ onClick GenChar ] [ text "Generate" ])
                        , centeredFlex
                            (form [ onSubmit (SaveChar player) ]
                                [ label []
                                    [ text "Username"
                                    , input
                                        [ type_ "text"
                                        , name "username"
                                        , onInput ChangePlayer
                                        ]
                                        []
                                    ]
                                , button [] [ text "Save" ]
                                ]
                            )
                        ]

                Show char ->
                    columns
                        [ column (summary char 200) 0.3
                        , column (portrait char 200) 1
                        , column (portrait char 200) 1
                        ]
            ]
    }
