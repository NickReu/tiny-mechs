module Character exposing (..)

import Json.Decode as Decode exposing (Decoder, int, string)
import Json.Decode.Pipeline exposing (optional, required)


type alias Character =
    { charId : String
    , charName : String
    , imgUrl : String
    , stats : MechStats
    }


type alias MechStats =
    { spark : Int, steel : Int }


charDecoder : Decoder Character
charDecoder =
    Decode.succeed Character
        |> required "id" string
        |> required "name" string
        |> required "img" string
        |> required "stats" mechStatsDecoder


mechStatsDecoder : Decoder MechStats
mechStatsDecoder =
    Decode.succeed MechStats
        |> optional "spark" int 1500
        |> optional "steel" int 1500
