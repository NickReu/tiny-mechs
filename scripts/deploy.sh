#!/bin/env bash

firebase deploy


iris='gcloud beta compute ssh --zone us-west1-b iris  --project smiley-sopel'
gscp='gcloud compute scp --zone us-west1-b --project smiley-sopel'

$iris --command "rm -f tiny_mechs/*.whl"
$gscp flask/dist/tiny_mechs*.whl nick@iris:~/tiny_mechs/
$iris --command "source tiny_mechs/.venv/bin/activate ; python3 -m pip install --upgrade tiny_mechs/*.whl ; sudo systemctl restart tiny_mechs"
