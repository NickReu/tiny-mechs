#!/bin/env bash


rm -rf dev/
mkdir dev/
cp -r elm/src/* dev/
sed -i 's|@@@@@|https://api.tiny-mechs.com|g' dev/*
elm reactor &

cd dev
elm make Main.elm --optimize --output dist/elm.js
cd ..
cp dev/dist/elm.js public/elm.js

cd flask
rm -rf dist
python3 setup.py bdist_wheel
