#!/bin/env bash

function cleanup {
    sleep 0.2
    echo
}

trap "exit \$exit_code" INT TERM
trap "exit_code=\$?; cleanup; kill 0" EXIT

rm -rf dev/
mkdir dev/
cp -r elm/src/* dev/
sed -i 's|@@@@@|http://localhost:5000|g' dev/*
elm reactor &

cd flask/src
FLASK_APP=tiny_mechs python3 -m flask run &
cd ../..

sleep 0.5
firefox --new-tab http://localhost:8000/dev/Main.elm &
wait
