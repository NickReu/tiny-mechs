from setuptools import setup

setup(
    packages=['tiny_mechs'],
    package_data={'tiny_mechs': ['data/textgen/*', 'data/imggen/*/*']},
    zip_safe=False,
    install_requires=['flask',
                      'Pillow',
                      'colormath',
                      'marshmallow_dataclass'],
    )
