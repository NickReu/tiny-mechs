import os

data_path = os.path.join('..', '..', 'data')
char_json_path = os.path.join(data_path, 'chardata', 'json')
char_img_path = os.path.join(data_path, 'chardata', 'imgs')
tempdata_path = os.path.join(data_path, 'tempdata')

pack_data_path = 'data'
imggen_path = os.path.join(pack_data_path, 'imggen')
textgen_path = os.path.join(pack_data_path, 'textgen')
