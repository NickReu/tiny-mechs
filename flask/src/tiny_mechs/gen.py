import random
import os
from uuid import uuid4
import tiny_mechs.paths as paths


def parse(pattern):
    '''Recursively, randomly fills out a pattern template'''
    pwords = pattern.split(' ')
    result = []
    for word in pwords:
        if word[0] != '$' and word[0] != '@':
            result.append(word)
        elif word[:2] == '$#':
            num = ''
            for _ in range(len(word) - 1):
                num += str(random.randint(0, 9))
            result.append(num)
        elif word[0] == '$':
            with open(os.path.join(paths.textgen_path, word[1:])) as lpfile:
                lines = lpfile.read().splitlines()
            ws = [1] * len(lines)
            for i, line in enumerate(lines):
                patterns = line.split(' ')
                if len(patterns) and patterns[-1] and patterns[-1][0] == '@':
                    ws[i] = int(patterns[-1][1:])
            lowerpattern = random.choices(lines, weights=ws)[0]
            result.append(parse(lowerpattern))
    return ' '.join(result)


def uuid():
    return uuid4().hex


def name():
    return parse('$TopLevelPattern')


def stats():
    spark, steel = 1000, 1000
    if random.random() < 0.67:
        if random.random() < 0.5:
            spark -= 500
            steel += 500
        else:
            spark += 500
            steel -= 500
    spark += random.randint(-50, 50)
    steel += random.randint(-50, 50)
    return spark, steel
