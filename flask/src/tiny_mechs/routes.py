from flask import send_file, request, Blueprint
import json
import os
import tiny_mechs.image as image
import tiny_mechs.gen as gen
import tiny_mechs.character as character
import tiny_mechs.paths as paths

api_app = Blueprint('api_app', __name__)


@api_app.route("/gen/char")
def get_new_char_json():
    char = character.Character.random()
    d = character.Character.Schema().dump(char)
    filename = f'char_{d["id"]}.json'
    json.dump(d, open(os.path.join(paths.tempdata_path, filename), 'w'))
    url = request.url_root + f'data/char/img/{d["img"]}.png'
    d['img'] = url
    return json.dumps(d)


@api_app.route("/save/char/<id>", methods=['POST'])
def save_char_data(id):
    filename = f'char_{id}.json'
    d = json.load(open(os.path.join(paths.tempdata_path, filename)))
    old = img_path_from_hash(d['img'], temp=True)
    new = img_path_from_hash(d['img'], temp=False)
    os.rename(old, new)
    filename = f'char_{d["id"]}.json'
    player = request.json['player']
    if not (len(player) > 2 and
            (player[0].isalpha or player[0].isnum) and
            (player[-1].isalpha or player[-1].isnum) and
            any(c.isalpha for c in player) and
            all(c.isalpha() or c == ' ' or c.isnum() for c in player)):
        return {'error': 'Invalid player name'}, 400
    json.dump(d, open(os.path.join(paths.char_json_path, filename), 'w'))
    url = request.url_root + f'data/char/img/{d["img"]}.png'
    d['img'] = url
    return json.dumps(d)


@api_app.route("/char/<id>")
def get_char_json(id):
    filename = f'char_{id}.json'
    d = json.load(open(os.path.join(paths.char_json_path, filename)))
    url = request.url_root + f'data/char/img/{d["img"]}.png'
    d['img'] = url
    return json.dumps(d)


@api_app.route("/data/char/img/<hash>.png")
def fetch_img(hash):
    path = img_path_from_hash(hash)
    if not os.path.exists(path):
        path = img_path_from_hash(hash, temp=True)
    return send_file(path, mimetype='image/gif')


@api_app.route("/gen/name")
def get_name():
    return gen.name()


@api_app.route("/gen/img")
def random_img_hash():
    return image.generate()[1]


def img_path_from_hash(hash, temp=False):
    filename = f'generated_{hash}.png'
    dir = paths.tempdata_path if temp else paths.char_img_path
    return os.path.join(dir, filename)
