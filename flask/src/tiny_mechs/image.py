from PIL import Image
from colormath.color_objects import LCHuvColor, sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000 as color_diff
import os
import random
import hashlib
import numpy as np
from itertools import combinations
import tiny_mechs.paths as paths


def get_hash(img):
    just_pixels = Image.new(img.mode, img.size)
    just_pixels.putdata(img.getdata())
    return hashlib.md5(just_pixels.tobytes()).hexdigest()


def generate():
    head = random_body_part('head')
    torso = random_body_part('torso')
    legs = random_body_part('legs')
    larm = random_body_part('larm')
    rarm = random_body_part('rarm')
    img = colored(combined(head, torso, legs, larm, rarm))
    hash = get_hash(img)
    filename = f'generated_{hash}.png'
    path = os.path.join(paths.tempdata_path, filename)
    if not os.path.exists(path):
        img.save(path)
    for p in head, torso, legs:
        p['i'].close()
    img.close()
    return filename, hash


def random_body_part(dirpart):
    dir = os.path.join(paths.imggen_path, dirpart)
    filenames = os.listdir(dir)
    parts = []
    parts_txt = [s[:-4] for s in filenames]
    for i, part_txt in enumerate(parts_txt):
        d = {'i': Image.open(os.path.join(dir, filenames[i])),
             't': dirpart}
        raw_props = part_txt.split('_')
        d['name'] = raw_props[0]
        for p in raw_props[1:]:
            prop_name = p.split('=')[0]
            prop_x = p.split('=')[1].split('-')[0]
            prop_y = p.split('=')[1].split('-')[1]
            d[prop_name] = {'x': int(prop_x), 'y': int(prop_y)}
        parts.append(d)
    return random.choice(parts)


def bounds(part):
    return (0,
            part['i'].height-part['size']['y'],
            part['size']['x'], part['i'].height)


def joint(a, p1, p2, j):
    return (a[0]+p1[j]['x']-p2[j]['x'],
            a[1]+p1['size']['y']-(p1[j]['y'] - 1)-p2['size']['y']+p2[j]['y'])


def combined(h, t, l, la, ra):
    i = Image.new('RGBA', (32, 32), (1, 1, 1, 0))
    anchor = (16, 0)
    neck = joint(anchor, h, t, 'neck')
    waist = joint(neck, t, l, 'waist')
    lsh = joint(neck, t, la, 'lsh')
    rsh = joint(neck, t, ra, 'rsh')
    i.alpha_composite(ra['i'], rsh, bounds(ra))
    i.alpha_composite(h['i'], anchor, bounds(h))
    i.alpha_composite(t['i'], neck, bounds(t))
    i.alpha_composite(l['i'], waist, bounds(l))
    i.alpha_composite(la['i'], lsh, bounds(la))
    rect = i.getbbox()
    cropped = i.crop(rect)
    cx, cy = cropped.size
    squared = Image.new('RGBA', (32, 32), (1, 1, 1, 0))
    squared.paste(cropped, (16 - cx//2, 32-cy))
    return squared


def l_spread():
    if random.random() > 0.75:
        return (random.triangular(0, 100),) * 3
    elif random.random() > 0.66:
        x = random.triangular(0, 75)
        return random.sample((random.triangular(75, 100), x, x), 3)
    elif random.random() > 0.5:
        x = random.triangular(25, 100)
        return random.sample((random.triangular(0, 25), x, x), 3)
    else:
        return random.sample((random.triangular(0, 33),
                              random.triangular(33, 66),
                              random.triangular(66, 100)
                              ), 3)


def c_spread():
    if random.random() > 0.5:
        return ((random.triangular(0, 132) + random.triangular(0, 132)) / 2,
                (random.triangular(0, 132) + random.triangular(0, 132)) / 2,
                (random.triangular(0, 132) + random.triangular(0, 132)) / 2)
    else:
        return (random.triangular(64, 132),
                random.triangular(0, 64),
                random.triangular(0, 64))


def h_spread():
    start = random.uniform(0, 360)
    angle = random.choice((0, 30, 120))
    hs = random.sample((start, start+angle, start+(angle*2)), 3)
    for i in range(3):
        if hs[i] > 360:
            hs[i] -= 360
    return hs


def rand_palette():
    ls = l_spread()
    cs = c_spread()
    hs = h_spread()
    colors = tuple([new_color(l, c, h) for l, c, h in zip(ls, cs, hs)])
    return colors


def new_color(l=None, c=None, h=None):
    if l is None:
        l = random.triangular(0, 100)
    if c is None:
        c = (random.triangular(0, 132) + random.triangular(0, 132)) / 2
    if h is None:
        h = random.uniform(0, 360)
    return LCHuvColor(l, c, h)


def darken(c, m=0.9, e=1.2):
    clamped = rgb_clamped(c)
    darkened = sRGBColor(*map(lambda v: ((v / 255) ** e) * m, clamped))
    return darkened


def min_diff(*colors):
    downscaled = [tuple([v / 255.0 for v in c]) for c in colors]
    rgbColors = [sRGBColor(*d) for d in downscaled]
    labColors = [convert_color(c, LabColor) for c in rgbColors]
    pairs = list(combinations(labColors, 2))
    x = None
    for pair in pairs:
        d = color_diff(*pair)
        if x is None or d < x:
            x = d
    return x


def rgb_clamped(c):
    if type(c) != sRGBColor:
        c = convert_color(c, sRGBColor)
    clamped = tuple([v * 255 for v in
                    (c.clamped_rgb_r,
                     c.clamped_rgb_g,
                     c.clamped_rgb_b)])
    return clamped


def colored(i):
    data = np.array(i)
    red, green, blue, alpha = data.T
    prim_a = (red == 255)
    dprim_a = (red > 0) & (red < 255) & (blue == 0) & (green == 0)
    sec_a = (green == 255)
    dsec_a = (green > 0) & (green < 255) & (blue == 0) & (red == 0)
    ter_a = (blue == 255)
    dter_a = (blue > 0) & (blue < 255) & (green == 0) & (red == 0)
    prim, sec, ter = rand_palette()
    tries = 10
    while min_diff(*map(rgb_clamped, (prim, sec, ter))) < 20 and tries > 0:
        prim, sec, ter = rand_palette()
        tries -= 1
    dprim, dsec, dter = darken(prim), darken(sec), darken(ter)
    data[..., :-1][prim_a.T] = rgb_clamped(prim)
    data[..., :-1][dprim_a.T] = rgb_clamped(dprim)
    data[..., :-1][sec_a.T] = rgb_clamped(sec)
    data[..., :-1][dsec_a.T] = rgb_clamped(dsec)
    data[..., :-1][ter_a.T] = rgb_clamped(ter)
    data[..., :-1][dter_a.T] = rgb_clamped(dter)
    return Image.fromarray(data)
