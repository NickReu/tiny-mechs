from flask import Flask
import logging
import click
import os
os.chdir(os.path.dirname(os.path.abspath(__file__)))
from tiny_mechs.routes import api_app  # NOQA
import tiny_mechs.paths as paths  # NOQA

log = logging.getLogger('werkzeug')
log.setLevel(logging.ERROR)


def secho(text, file=None, nl=None, err=None, color=None, **styles):
    pass


def echo(text, file=None, nl=None, err=None, color=None, **styles):
    pass


click.echo = echo
click.secho = secho

app = Flask(__name__)

data_paths = (paths.char_img_path, paths.char_json_path, paths.tempdata_path)
for path in data_paths:
    if not os.path.exists(path):
        os.makedirs(path)
for file in os.scandir(paths.tempdata_path):
    os.remove(file.path)


@app.after_request
def after_request(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
    return response


app.register_blueprint(api_app)
