from marshmallow_dataclass import dataclass
import tiny_mechs.gen as gen
import tiny_mechs.image as image


@dataclass()
class Stats:
    spark: int
    steel: int


@dataclass()
class Character:
    id: str
    player: str
    name: str
    img: str
    stats: Stats

    @classmethod
    def random(self):
        self.id = Character.new_id()
        self.name = Character.new_name()
        self.img = Character.new_img()
        self.stats = Character.new_stats()
        return self

    @classmethod
    def new_id(cls):
        return gen.uuid()

    @classmethod
    def new_name(cls):
        return gen.name()

    @classmethod
    def new_img(cls):
        return image.generate()[1]

    @classmethod
    def new_stats(cls):
        return Stats(*gen.stats())

    #     self = Character()
    #     if dict:
    #         self.id = id
    #         self.owner = dict['owner']
    #         self.name = dict['name']
    #         self.strategy = dict['strategy']
    #         self.age = dict['age']
    #         self.gender = dict['gender']
    #         self.stats = dict['stats']
    #         self.mech = await Mech.async_init(dict = dict['mech'])
    #         self.health = dict['health']
    #         self.history = dict['history'][-100:]
    #         self.record = dict['record']
    #         if 'money' in dict:
    #             self.money = dict['money']
    #         else:
    #             self.money = 0
    #         if 'pet' in dict:
    #             try:
    #                 self.pet = Pet(dict['pet']['type'],
    #                                dict['pet']['name'],
    #                                dict['pet']['rarity'])
    #             except:
    #                 self.pet = Pet(dict['pet']['type'],
    #                                dict['pet']['name'],
    #                                1)
    #         else:
    #             self.pet = Pet(None)
    #         if 'bday' in dict:
    #             self.bday = dict['bday']
    #         else:
    #             self.bday = await get_time_days() + random.randint(0,366)
    #         if 'importanthistory' in dict:
    #             self.importanthistory = dict['importanthistory'][-100:]
    #         else:
    #             self.importanthistory = []
    #         if 'rank' in dict:
    #             self.rank = dict['rank']
    #         else:
    #             self.rank = 0
    #         if self.bday <= await get_time_days():
    #             self.age += 1
    #             self.bday += 366
    #             await self.add_history('Celebrated ' + \
    #                await self.pronoun(type='his/her') + ' ' + \
    #                await ord(self.age) + ' birthday!', True)
    #             await updatechar(self)
    #         if self.record >= 6*(self.rank + 1)*(1.11**self.rank):
    #             await self.promote()
    #             await self.add_history('Pilot promoted!', True)
    #             await updatechar(self)
    #
    # def __init__(self):
    #     pass
    #
    # @classmethod
    # async def statname(cls, stat):
    #     return {0: 'Head',
    #             1: 'Heart',
    #             2: 'Strength',
    #             3: 'Speed'}[stat]
    #
    # async def get_dict_for_json(self):
    #     return {'owner': self.owner, 'name': self.name,
    #             'strategy': self.strategy, 'age': self.age,
    #             'gender': self.gender, 'stats': self.stats,
    #             'mech': await self.mech.get_dict_for_json(),
    #             'health': self.health, 'history': self.history,
    #             'record': self.record, 'money': self.money,
    #             'pet': await self.pet.get_dict_for_json(),
    #             'bday': self.bday, 'rank': self.rank,
    #             'importanthistory': self.importanthistory}
    #
    # async def summary(self):
    #     ranks = ['Recruit','Pilot','Pilot First Class',
    #              'Senior Pilot','Petty Sergeant','Sergeant',
    #              'Master Sergeant', 'Master Sergeant First Class',
    #              'Ensign', 'Lieutenant', 'Captain', 'Major',
    #              'Lieutenant Colonel', 'Colonel', 'Lieutenant Commander',
    #              'Commander', 'Brigadier General', 'Major General',
    #              'Lieutenant General', 'General, 1 Star']
    #     if self.rank < len(ranks):
    #         rank = ranks[self.rank]
    #     else:
    #         stars = self.rank - len(ranks) + 1
    #         rank = 'General, ' + str(stars) + ' Stars'
    #     return 'Pilot:\n' + self.name + ', piloting the ' + \
    #            self.mech.name + '\n' + \
    #            'Player: '    + self.owner             + '\n' + \
    #            'Age: '       + str(self.age)          + '\n' + \
    #            'Gender: '    + self.gender            + '\n' + \
    #            'Rank: '      + rank                   + '\n' + \
    #            'Strategy: '  + self.strategy          + '\n' + \
    #            'Health: '    + self.health            + '\n' + \
    #            'Money: '     + str(self.money)        + '\n' + \
    #            'Companion: ' + await self.pet.print() + '\n' + \
    #            'Stats: '                              + '\n' + \
    #            '   Head: '     + str(self.stats[0])   + '\n' + \
    #            '   Heart: '    + str(self.stats[1])   + '\n' + \
    #            '   Strength: ' + str(self.stats[2])   + '\n' + \
    #            '   Speed: '    + str(self.stats[3])
    #
    # async def pronoun(self, type):
    #     if type == 'he/she':
    #         if self.gender == 'Male':
    #             return 'he'
    #         elif self.gender == 'Female':
    #             return 'she'
    #         else:
    #             return 'they'
    #     elif type == 'him/her':
    #         if self.gender == 'Male':
    #             return 'him'
    #         elif self.gender == 'Female':
    #             return 'her'
    #         else:
    #             return 'them'
    #     elif type == 'his/her':
    #         if self.gender == 'Male':
    #             return 'his'
    #         elif self.gender == 'Female':
    #             return 'her'
    #         else:
    #             return 'their'
    #     elif type == 'his/hers':
    #         if self.gender == 'Male':
    #             return 'his'
    #         elif self.gender == 'Female':
    #             return 'hers'
    #         else:
    #             return 'theirs'
    #     else:
    #         return ''
    #
    # async def add_history(self, text, is_important=False):
    #     if is_important:
    #         self.importanthistory
    #             .append(await get_time_string() + ': ' + text)
    #     self.history.append(await get_time_string() + ': ' + text)
    #
    # async def get_history(self, is_important=False):
    #     if is_important:
    #         to_ret = 'Recent important history for ' + self.name + \
    #                  ' as of ' + (await get_time_string()).lower() + ':'
    #         for event in self.importanthistory[-8:]:
    #             to_ret += '\n' + event
    #     else:
    #         to_ret = 'Recent history for ' + self.name + \
    #                  ' as of ' + (await get_time_string()).lower() + ':'
    #         for event in self.history[-8:]:
    #             to_ret += '\n' + event
    #     return to_ret
    #
    # async def promote(self):
    #     prom_stats = random.choices(range(4), k=3)
    #     for i, s in enumerate(prom_stats):
    #         self.stats[s] += 10*(i+1)
    #     self.rank += 1
